from django.http import HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse, reverse_lazy
from django.shortcuts import get_object_or_404, render
from django.views import generic
from django.views.generic.edit import CreateView
from django.utils import timezone

import forms
from .models import Vendor, Order


# View for homepage
def index(request):
    latest_orders = Order.objects.order_by("pub_date")[:5]

    context = {
        'latest_orders': latest_orders,
    }

    return render(request, "OrderVan/index.html", context)


class OrdersResultsView(LoginRequiredMixin, generic.ListView):
    model = Order
    template_name = 'orderVan/order_results.html'
    context_object_name = "latest_orders"
    paginate_by = 10

    def get_queryset(self):
        orderby = self.request.GET.get("orderby", '-pub_date')
        filter_val = self.request.GET.get('filter_by_status', None)

        orders = Order.objects.all()
        if filter_val:
            orders = orders.filter(general_status=filter_val)
        orders = orders.order_by(orderby)
        return orders

    def get_context_data(self, **kwargs):
        context = super(OrdersResultsView, self).get_context_data(**kwargs)
        context['form'] = forms.SearchForm

        return context


class OrdersDetailView(LoginRequiredMixin, generic.UpdateView):
    model = Order
    template_name = 'orderVan/order_details.html'

    # context_object_name is implicitly given to be "order"

    def get_form_class(self):
        return forms.OrderDetailsForm


class NewOrder(LoginRequiredMixin, CreateView):
    model = Order
    fields = "__all__"
    template_name = 'orderVan/new_order.html'
    # Need to reverse lazy because this will be run before the url conf is loaded
    success_url = reverse_lazy('OrderVan:order_results')

    def form_invalid(self, form):
        response = super(NewOrder, self).form_invalid(form)
        return response

    def form_valid(self, form):
        order = form.save(commit=False)
        order.pub_date = timezone.now()
        order.created_by = self.request.user
        order.save()
        response = super(NewOrder, self).form_valid(form)
        return response

    def get_form_class(self):
        return forms.NewOrderForm


def change_order_status(request, order_id):
    order = get_object_or_404(Order, id=order_id)

    try:
        order.general_status = Order.GENERAL_STATUSES[int(request.POST["general_status"]) - 1][1]
        print("changing general status: ", order.general_status)
    except (KeyError, Order.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'OrderVan/change_order_status.html', {
            'order': order,
            'error_message': "You didn't select a choice.",
        })

    order.save()

    return HttpResponseRedirect(reverse('OrderVan:order_details', args=(order.id,)))
