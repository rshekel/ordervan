from django.contrib import admin
from .models import Vendor, Order, ProjectCategory, Project, Manager

# Register your models here.
admin.site.register(Vendor)
admin.site.register(Order)
admin.site.register(ProjectCategory)
admin.site.register(Project)
admin.site.register(Manager)
