from django.forms import ModelForm, Textarea, Form, ChoiceField, CharField, SelectDateWidget
from models import Order


class NewOrderForm(ModelForm):

    class Meta:
        model = Order
        fields = ["title", "description", "vendor", "price", "currency", "manager", "quote"]
        widgets = {
            'description': Textarea(attrs={'rows': 10})
        }

    def __init__(self, *args, **kwargs):
        super(NewOrderForm, self).__init__(*args, **kwargs)
        self.fields['quote'].required = False


class OrderDetailsForm(ModelForm):

    class Meta:
        model = Order
        fields = "__all__"
        widgets = {
            'date_sent_order': SelectDateWidget,
            'estimated_delivery': SelectDateWidget,
            'receipt_date': SelectDateWidget
        }


class SearchForm(Form):

    filter_by_status = ChoiceField(choices=Order.GENERAL_STATUSES,
                                   required=False, widget=None, label="Filter By General Status")

    fields_to_filter = [(f.name, f.name) for f in Order._meta.get_fields()]
    orderby = ChoiceField(required=False, choices=fields_to_filter)
