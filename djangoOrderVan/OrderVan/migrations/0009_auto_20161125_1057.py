# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-11-25 08:57
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('OrderVan', '0008_auto_20161125_1039'),
    ]

    operations = [
        migrations.CreateModel(
            name='Manager',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AlterModelOptions(
            name='projectcategory',
            options={'verbose_name_plural': 'project categories'},
        ),
        migrations.AlterField(
            model_name='order',
            name='manager',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='manage_user', to='OrderVan.Manager'),
        ),
        migrations.AlterField(
            model_name='projectcategory',
            name='father_project',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='OrderVan.Project'),
        ),
    ]
