from django.conf.urls import url

from . import views

app_name = 'OrderVan'

urlpatterns = [
    url(r'^$', views.index, name='index'),

    # Orders urls
    url(r'^orders/?$', views.OrdersResultsView.as_view(), name='order_results'),

    # The () "capture" the result of regex.
    # The ?P<X> is the name og the kwarg
    # The trailing /? is for it to work with trailing slash or without
    url(r'^orders/(?P<pk>[0-9]+)/details/?$', views.OrdersDetailView.as_view(), name='order_details'),
    url(r'^orders/(?P<order_id>[0-9]+)/change_status/?$', views.change_order_status, name='change_order_status'),
    url(r'^orders/new_order/?$', views.NewOrder.as_view(), name='new_order'),

    # Vendors urls
    # url(r'^vendors/(?P<vendor_id>[0-9]+)/$', views.vendor_details, name='vendor_details'),
    # url(r'vendors$', views.all_vendors, name='all_vendors'),

]

